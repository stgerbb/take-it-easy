import React, { Component } from 'react';
import joker from './assets/unblock_hexagon.png'
import './HexagonUnblocker.css';

interface HexagonUnblockerProps {
    unblockScore: string,
    hexagonUnblockerCallback: (hexagonUnblocker: HexagonUnblocker) => void,
    onClickCallack: (hexagonUnblocker: HexagonUnblocker)  => void
}

interface HexagonUnblockerState {
    enabled: boolean,
}

export default class HexagonUnblocker extends Component<HexagonUnblockerProps, HexagonUnblockerState>  {

    private used: boolean = false;
    private clicked: boolean = false;

    constructor(props: HexagonUnblockerProps) {
        super(props);

        this.state = {
            enabled: false,
        }
    }

    componentDidMount() {
        this.props.hexagonUnblockerCallback(this);
    }

    setClicked = (bool: boolean) => this.clicked = bool;

    isClicked = () => this.clicked;

    isUsed = () => this.used;

    setUsed = (bool: boolean) => this.used = bool; 

    isEnabled = () => this.state.enabled;

    setEnabled = (bool: boolean) => { 
        this.setState({
            enabled: bool
        })
     }

    render () {
        return (
            <div>
                <img alt="Hexagon unblocker" src={joker} style={this.state.enabled ? { opacity: "100%" } : { opacity: "25%" }} onClick={() => this.props.onClickCallack(this)} />
                <h4 style={this.state.enabled ? { opacity: "100%" } : { opacity: "25%" }}>{this.props.unblockScore}</h4>
            </div>
        );
    }
}