import { Table } from './Interfaces';

export default class Grid extends Array<Table> {
    hexagonTables: Array<Table> = [
        { column: 0, row: 0 },
        { column: -1, row: 1 },
        { column: 0, row: 1 },
        { column: 1, row: 0 },
        { column: -2, row: 2 },
        { column: -1, row: 2 },
        { column: 0, row: 2 },
        { column: 1, row: 1 },
        { column: 2, row: 0 },
        { column: -2, row: 3 },
        { column: -1, row: 3 },
        { column: 0, row: 3, },
        { column: 1, row: 2 },
        { column: 2, row: 1 },
        { column: -2, row: 4 },
        { column: -1, row: 4 },
        { column: 0, row: 4 },
        { column: 1, row: 3 },
        { column: 2, row: 2 }
    ];

    constructor() {
        super();
        this.hexagonTables.forEach(hexagonTable => {
            this.push(hexagonTable);
        })
    }
}