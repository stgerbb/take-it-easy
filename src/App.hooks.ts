import { wrap, releaseProxy } from 'comlink';
import { useEffect, useState, useMemo } from 'react';

import Grid from './Grid';
import Hexagons from './Hexagons';

/**
 * Hook that performs the calculation on the worker
 */
export const useCalculateMinimalNumberOfMovesToReachGoal = (
    goal: number,
    hexagons: Hexagons,
    grid: Grid, 
    calculateMinimalNumberOfMoves: boolean,
): {isCalculating: boolean, result: number} => {
    const [data, setData] = useState({
        isCalculating: false,
        result: undefined as number | undefined
    });

    const { workerApi } = useWorker();

    useEffect(() => {
        if(calculateMinimalNumberOfMoves) {
            setData({isCalculating: true, result: undefined});
            workerApi.calculateMinimalNumberOfMovesToReachGoal(goal, hexagons, grid).then(result => setData({isCalculating: false, result})).then(() => data);
        }
    }, [workerApi, setData, goal, hexagons, grid, calculateMinimalNumberOfMoves]);

    return data;
}

const useWorker = () => {
    const workerApiAndCleanup = useMemo(() => makeWorkerApiAndCleanup(), []);

    useEffect(() => {
        const { cleanup } = workerApiAndCleanup;

        return () => {
            cleanup();
        };
    }, [workerApiAndCleanup]);
    return workerApiAndCleanup;
}

const makeWorkerApiAndCleanup = () => {
    const worker = new Worker('./workers', {
        name: 'workers',
        type: 'module'
    });
    const workerApi = wrap<import('./workers').CalculationWorker>(worker);

    const cleanup = () => {
        workerApi[releaseProxy]();
        worker.terminate();
    }

    const workerApiAndCleanup = {workerApi, cleanup};

    return workerApiAndCleanup
}