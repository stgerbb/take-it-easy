import Combinatorics from 'js-combinatorics';

import Hexagons from './Hexagons';
import Grid from './Grid';
import { calculateGridScore } from './GridUtils';
import Hexagon from './Hexagon';

export const calculateMinimalNumberOfMovesToReachGoal = (goal: number, hexagons: Hexagons, grid: Grid): number => {
    let minimalNumberOfRequiredMoves: number = -1;
    backtrackingLoop:
    for (let n: number = 2; n <= hexagons.length; n++) {
        // get all combinations of selecting n distinct hexagons from all hexagons
        let hexagonsToSwap = generateCombinationsOfNDistincHexagons(hexagons, n).filter((hexagons: Hexagon[]) => !hexagons.some((hexagon: Hexagon) => hexagon.cardValues.blocked))
        for(const hexagonsToSwapEntry of hexagonsToSwap) {
            if(swappingOrderAffectsScore(n)) {
                const hexagonsToSwapPermutations = generatePermutations(hexagonsToSwapEntry);
                for (const hexagonsToSwapPermutation of hexagonsToSwapPermutations) {
                    const score = swapSelectedHexagonsAndCalculateScore(hexagonsToSwapPermutation, hexagons, grid);
                    if (score >= goal) {
                        console.log('iteration: ' + n)
                        console.log(hexagonsToSwapPermutation)
                        minimalNumberOfRequiredMoves = n-1;
                        // break out of loop with label backtrackingLoop several layers up
                        break backtrackingLoop;
                    }  
                }
            } else {
                const score = swapSelectedHexagonsAndCalculateScore(hexagonsToSwapEntry, hexagons, grid);
                if (score >= goal) {
                    console.log('iteration: ' + n)
                    console.log(hexagonsToSwapEntry);
                    minimalNumberOfRequiredMoves = n-1;
                    break backtrackingLoop;
                }  
            }
        }
    }
    return minimalNumberOfRequiredMoves;
}

const generateCombinationsOfNDistincHexagons = (hexagons: Hexagons, n: number) => {
    return Combinatorics.combination(hexagons, n).toArray();
}

const generatePermutations = (hexagons: Hexagon[]) => Combinatorics.permutation(hexagons).toArray()

// swapping of two hexagons always results in the same regardless of the swapping order
// swapping of more than two hexagons can result in different scores depending on the order --> permutations
const swappingOrderAffectsScore = (numberOfSwaps: number) => numberOfSwaps > 2

const swapSelectedHexagonsAndCalculateScore = (selectedHexagons: Array<Hexagon>, hexagons: Hexagons, grid: Grid): number => {
    carryOutAllSwaps(selectedHexagons, hexagons);
    const score = calculateGridScore(grid, hexagons);
    console.log(score);
    revertAllSwaps(selectedHexagons, hexagons);
    return score
}

const carryOutAllSwaps = (hexagonsToSwap: Array<Hexagon>, hexagons: Hexagons): void => {
    for (let i: number = 0; i < hexagonsToSwap.length-1; i++) {
        let temp = hexagonsToSwap[i].cardValues;
        hexagonsToSwap[i].cardValues = hexagonsToSwap[i + 1].cardValues;
        hexagonsToSwap[i + 1].cardValues = temp;
    }
}

const revertAllSwaps = (hexagonsToSwap: Array<Hexagon>, hexagons: Hexagons): void => {
    for (let i: number = hexagonsToSwap.length-1; i > 0; i--) {
        let temp = hexagonsToSwap[i].cardValues;
        hexagonsToSwap[i].cardValues = hexagonsToSwap[i - 1].cardValues;
        hexagonsToSwap[i - 1].cardValues = temp;
    }
}