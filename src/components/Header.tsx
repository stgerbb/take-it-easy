import Navbar from 'react-bootstrap/Navbar'
import React, { FunctionComponent } from 'react'
import { Link } from 'react-router-dom';
import '../App.css';

export const Header: FunctionComponent = () =>
<Navbar bg='dark' variant='dark' expand='lg'>
    <Navbar.Brand><Link className="navbar-links" to="/">Main menu</Link></Navbar.Brand>
</Navbar>
