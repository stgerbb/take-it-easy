import React from 'react';
import congratulations from '../assets/congratulations.png'
import './WinModal.css';

import { Button, Modal } from 'react-bootstrap'

interface WinModalProps {
    show: boolean
    setShow: (show: boolean) => void
    initialiseNewGame: () => void
}

function WinModal(props: WinModalProps) {

    const handleClose = () => props.setShow(false);
    const startNewGame = () => {
        props.initialiseNewGame();
        handleClose();
    }

    return (
        <>
            <Modal show={props.show} onHide={handleClose} size='lg'>
                <Modal.Header closeButton>
                    <Modal.Title>You won!</Modal.Title>
                </Modal.Header>
                <Modal.Body className='modal-body'>
                    <img src={congratulations} />
                    <p>Woohoo, you have reached the goal! Congratulations!</p>
                </Modal.Body>
                <Modal.Footer id='custom-modal-footer'>
                    <Button variant="primary" size="lg" onClick={startNewGame}>
                        Start a new game
                    </Button>
                    <Button variant="primary" href='/' size="lg" onClick={handleClose}>
                        Go back to the homepage
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default React.memo(WinModal);