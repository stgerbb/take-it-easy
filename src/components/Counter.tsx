import React, { FunctionComponent } from 'react';
import './Counter.css'

type CounterProps = {
    value: string
}
export const Counter: FunctionComponent<CounterProps> = ({ value, children }) =>
    <div className='Counter-container'>
        <h4>{children}:</h4>
        <p>{value}</p>
    </div>