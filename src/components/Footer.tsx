import Navbar from 'react-bootstrap/Navbar'
import React, { FunctionComponent } from 'react'
import '../App.css';

export const Footer: FunctionComponent = () =>
<Navbar bg='dark' variant='dark' expand='lg' fixed="bottom">
    <Navbar.Brand>&copy; 2020 Melanie Calame &amp; Stefan Gerber</Navbar.Brand>
</Navbar>
