import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import GamePage from './GamePage';
import HomePage from './HomePage';
import './App.css';

const App: FunctionComponent<{}> = (_) => {

  const [difficultyLevel, setDifficultyLevel] = React.useState(-1);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage setDifficultyLevel={setDifficultyLevel} />
        </Route>
        <Route exact path="/game">
          <GamePage difficultyLevel={difficultyLevel} />
        </Route>
      </Switch>
    </Router>
  );
}

export default React.memo(App);