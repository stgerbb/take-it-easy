import Hexagon from './Hexagon';
import Grid from './Grid'
import { Coordinates, CardValues, Table } from './Interfaces'
import CardGenerator from './CardGenerator';

export default class Hexagons extends Array<Hexagon> {    
    constructor(grid: Grid, size: number, difficultyLevel: number,  drawingStartPoint: Coordinates) {
        super();
        const gridSize = grid.length;
        let cards = this.getCardsForHexagons(gridSize)
        cards = this.blockCardsAccordingToDifficultyLevel(cards, difficultyLevel);
        for (let i: number = 0; i < gridSize; i++) {
            const hexagon: Hexagon = new Hexagon(`hexagon-${i}`, grid[i], size, drawingStartPoint, cards[i]);
            this.push(hexagon);
        }
    }

    getCardsForHexagons = (gridSize: number): Array<CardValues> => {
        const cardGenerator = new CardGenerator();
        return cardGenerator.getNRandomlySelectedPossibleCards(gridSize);
    }

    blockCardsAccordingToDifficultyLevel = (cards: Array<CardValues>, difficultyLevel: number):  Array<CardValues> => {
        const blockedCards = cards.slice(0, difficultyLevel).map((card: CardValues) => {
            card.blocked = true;
            return card;
        });
        const unblockedCards = cards.slice(difficultyLevel, cards.length).map((card: CardValues) => {
            card.blocked = false;
            return card;
        });
        const updatedCards = blockedCards.concat(unblockedCards);
        const shuffledCards = this._shuffleCards(updatedCards);
        return shuffledCards;
    }

    _shuffleCards(cards: Array<CardValues>) {
        for (let i = cards.length - 1; i > 0; i--) {
          let j = Math.floor(Math.random() * (i + 1));
          [cards[i], cards[j]] = [cards[j], cards[i]];
        }
        return cards;
      }

    unselectAllHexagons = (): void => {
        this.forEach(element => {
            if (element.isSelected()) {
                element.setSelected(false);
            }
        });
    }

    getHexagon = (hexagon: Hexagon): Hexagon => {
        const index = this.indexOf(hexagon);
        return this[index];
    }

    getSelectedHexagons = (): Array<Hexagon> => {
        let selectedHexagons: Array<Hexagon> = [];
        this.forEach(element => {
            if (element.isSelected()) {
                selectedHexagons.push(element);
            }
        });
        return selectedHexagons;
    }

    getUnSelectedHexagons = (): Array<Hexagon> => {
        let unselectedHexagons: Array<Hexagon> = [];
        this.forEach(element => {
            if (!element.isSelected()) {
                unselectedHexagons.push(element);
            }
        });
        return unselectedHexagons;
    }

    swapHexagonContent = (a: Hexagon, b: Hexagon): void => {
        const indexA = this.indexOf(a);
        const indexB = this.indexOf(b);
        const temp: CardValues = this[indexA].getCardValues();
        this[indexA].setCardValues(this[indexB].getCardValues());
        this[indexB].setCardValues(temp);
    }

    setGridPosition = (hexagon: Hexagon, gridPostition: Table): void => {
        hexagon.setGridPosition(gridPostition);
    }
}