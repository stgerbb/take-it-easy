import Hexagon from './Hexagon';
//@ts-ignore
import anime from 'animejs/lib/anime.es.js';

export const animateHexagon = (hexagon: Hexagon) => {
    anime({
    targets: `.${hexagon.getId()}`,
    scale: [
      {value: .10, duration: 0},
      {value: 1, duration: 600}
    ],
    delay: anime.stagger(0)
  });
}