import HexagonSvg from './HexagonSvg';

export default class HexagonSvgCache extends Array<HexagonSvg> {

    save = (svg: HexagonSvg): void => {
        this.push(svg);
    }

    clear = () => {
        while(this.length) {
            this.pop();
        }
    }
}