import { } from '@testing-library/react';

import Hexagons from '../Hexagons';
import Grid from '../Grid';
import { CardValues } from '../Interfaces';
import Hexagon from '../Hexagon';
import { calculateGridScore } from '../GridUtils'
import { Difficulty } from '../Difficulty';

const grid: Grid = new Grid();
const hexagons: Hexagons = new Hexagons(grid, 70, Difficulty.Easy, { x: 200, y: 100 });

test('Grid consists of 19 hexagons', () => {
    expect(hexagons.length).toBe(19);
});

test('Swap card values of two hexagons', () => {
    const hexagon1 = hexagons[0];
    const hexagon2 = hexagons[1];
    const cardValuesHexagon1: CardValues = hexagon1.getCardValues();
    const cardValuesHexagon2: CardValues = hexagon2.getCardValues();
    hexagons.swapHexagonContent(hexagon1, hexagon2);
    expect(hexagon1.getCardValues()).toStrictEqual(cardValuesHexagon2);
    expect(hexagon2.getCardValues()).toStrictEqual(cardValuesHexagon1);
});

test('Unselect all hexagons', () => {
    let counter: number = 0;
    hexagons.forEach(element => {
        if (element.isSelected()) {
            element.setSelected(false);
        }
    });
    hexagons.forEach(element => {
        if (!element.isSelected()) {
            counter++;
        }
    });
    expect(counter).toBe(19);
});

test('Get selected hexagons', () => {
    hexagons[0].setSelected(true);
    hexagons[1].setSelected(true);
    const slctHexagons: Array<Hexagon> = hexagons.getSelectedHexagons();
    expect(slctHexagons.length).toBe(2);
});

test('Each hexagon has a card', () => {
    let counter: number = 0;
    hexagons.forEach(element => {
        let cardValues: CardValues = element.getCardValues();
        if (cardValues !== null) {
            counter++
        }
    });
    expect(counter).toBe(19);
});

const resetCardValues = () => {
    for (let i: number = 0; i < hexagons.length; i++) {
        hexagons[i].setCardValues({ x: 0, y: 0, z: 0, blocked: false });
    }
}

test('Calculate column score', () => {
    resetCardValues();
    hexagons[0].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[2].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[6].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[11].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[16].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    expect(calculateGridScore(grid, hexagons)).toBe(25);
});

test('Calculate row score', () => {
    resetCardValues();
    hexagons[0].setCardValues({ x: 0, y: 10, z: 0, blocked: false });
    hexagons[3].setCardValues({ x: 0, y: 10, z: 0, blocked: false });
    hexagons[8].setCardValues({ x: 0, y: 10, z: 0, blocked: false });
    expect(calculateGridScore(grid, hexagons)).toBe(30);
});

test('Calculate diagonal score', () => {
    resetCardValues();
    hexagons[0].setCardValues({ x: 12, y: 0, z: 0, blocked: false });
    hexagons[1].setCardValues({ x: 12, y: 0, z: 0, blocked: false });
    hexagons[4].setCardValues({ x: 12, y: 0, z: 0, blocked: false });
    expect(calculateGridScore(grid, hexagons)).toBe(36);
});

test('Calculate grid score', () => {
    resetCardValues();
    hexagons[0].setCardValues({ x: 12, y: 10, z: 5, blocked: false });
    hexagons[1].setCardValues({ x: 12, y: 0, z: 0, blocked: false });
    hexagons[2].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[3].setCardValues({ x: 0, y: 10, z: 0, blocked: false });
    hexagons[4].setCardValues({ x: 12, y: 0, z: 0, blocked: false });
    hexagons[6].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[8].setCardValues({ x: 0, y: 10, z: 0, blocked: false });
    hexagons[11].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    hexagons[16].setCardValues({ x: 0, y: 0, z: 5, blocked: false });
    expect(calculateGridScore(grid, hexagons)).toBe(91);
});