import { } from '@testing-library/react';
import Hexagon from '../Hexagon';
import { Coordinates, Table, CardValues } from '../Interfaces';

const hexagon: Hexagon = new Hexagon('hexagon-0', { row: 0, column: 0 }, 70, { x: 200, y: 200 }, { x: 1, y: 2, z: 3, blocked: false });

test('Hexagon consists of six corners', () => {
    expect(hexagon.length).toBe(6)
});

test('Hexagon is on creation unselected', () => {
    expect(hexagon.isSelected()).toBe(false);
});

test('Hexagon consists of three card values', () => {
    expect(hexagon.getCardValues()).toStrictEqual({ x: 1, y: 2, z: 3, blocked: false });
});

test('Hexagon has grid position', () => {
    expect(hexagon.getGridPosition()).toStrictEqual({ row: 0, column: 0, s: -0 });
});

test('Hexagon is selected', () => {
    hexagon.setSelected(true);
    expect(hexagon.isSelected()).toBe(true);
});

test('Hexagon is unselected', () => {
    hexagon.setSelected(false);
    expect(hexagon.isSelected()).toBe(false);
});

test('Calculate hexagon corner', () => {
    const corner: Coordinates = hexagon.calcCornerOfHexagon({ x: 105, y: 105 }, 70, 3, { x: 200, y: 100 });
    expect(corner).toStrictEqual({ x: 235, y: 205 });
});

test('Set card values of Hexagon', () => {
    const newCardValues: CardValues = { x: 5, y: 7, z: 9, blocked: false };
    hexagon.setCardValues(newCardValues);
    expect(hexagon.getCardValues()).toStrictEqual(newCardValues);
});