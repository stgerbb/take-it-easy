import {CardValues} from './Interfaces';
import Combinatorics from 'js-combinatorics';

export default class CardGenerator {

    private readonly xCardValues = [2, 6, 7]
    private readonly yCardValues = [3, 4, 8]
    private readonly zCardValues = [1, 5, 9]

    private generateAllPossibleCards(): Array<CardValues> {
        let cards: Array<CardValues> = new Array<CardValues>();
        const cmb = Combinatorics.cartesianProduct(this.xCardValues, this.yCardValues, this.zCardValues);
        cmb.toArray().forEach((combination: number[]) => {
            cards.push({x: combination[0], y: combination[1], z: combination[2], blocked: false})
        });
        return cards
    }

    private randomlySelectNCards(cards: Array<CardValues>, numberOfCards: number) {
        const shuffledCards = cards.sort(() => 0.5 - Math.random());
        return shuffledCards.slice(0, numberOfCards);
        
    }

    getNRandomlySelectedPossibleCards(numberOfCards: number): Array<CardValues> {
        const cards = this.generateAllPossibleCards();
        return this.randomlySelectNCards(cards, numberOfCards);
    }

}