import React, { FunctionComponent } from 'react';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Spinner from 'react-bootstrap/Spinner'
import { Counter } from './components/Counter'
import { Header } from './components/Header'
import HexagonGrid from './HexagonGrid';
import { Redirect } from 'react-router';
import './App.css';
import HexagonUnblocker from './HexagonUnblocker';
import HexagonSvg from './HexagonSvg';
import { Button } from 'react-bootstrap';
import Hexagon from './Hexagon'
import { animateHexagon } from './animateUtils';
import { Difficulty } from './Difficulty';

export const calculateRandomNumber = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

interface GamePageProps {
  difficultyLevel: number
}

const GamePage: FunctionComponent<GamePageProps> = ({ difficultyLevel }) => {

  const hexgonUnblockers: Array<HexagonUnblocker> = [];

  const initGoal = (difficultyLevel: number): number => {
    switch (difficultyLevel) {
      case Difficulty.Easy:
        return calculateRandomNumber(50, 60);
      case Difficulty.Medium:
        return calculateRandomNumber(60, 70);
      case Difficulty.Hard:
        return calculateRandomNumber(70, 80);
      default:
        return 50;
    }
  }

  const [goal, setGoal] = React.useState(initGoal(difficultyLevel));
  const [score, setScore] = React.useState(0);
  const [minimumNumberOfMoves, setMinimumNumberOfMoves] = React.useState(-1);
  const [currentNumberOfMoves, setCurrentNumberOfMoves] = React.useState(0);
  const [goalReached, setGoalReached] = React.useState(false);
  const [hexagonUnblocker, setHexagonUnblocker] = React.useState([]);
  const [blockedHexagons, setBlockedHexagons] = React.useState([]);
  const [clickedHexagonUnblocker, setClickedHexagonUnblocker] = React.useState(undefined);
  const [calculateMinimalNumberOfRequiredMoves, setCalculateMinimalNumberOfRequiredMoves] = React.useState(false);
  const [calculationOngoing, setCalculationOngoing] = React.useState(false);
  const [showMinimalNumberOfRequiredMoves, setShowMinimalNumberOfRequiredMoves] = React.useState(false);

  React.useEffect(() => {
    enableHexagonUnblocker(score);
    setGoalReached(isGoalReached(goal, score));
  }, [score, goal]);

  const hexagonToBeUnblocked = (scoreValue: number, i: number, hexagonUnblocker: HexagonUnblocker): boolean => {
    return isScoreForUnblockingReached(scoreValue, i) && !hexagonUnblocker.isEnabled() && !hexagonUnblocker.isUsed()
  }

  const enableHexagonUnblocker = (scoreValue: number) => {
    for (let i: number = 0; i < hexagonUnblocker.length; i++) {
      if (hexagonToBeUnblocked(scoreValue, i, hexagonUnblocker[i])) {
        hexagonUnblocker[i].setEnabled(true);
      }
    }
  }

  const onChangeHandler = (scoreValue: number): void => {
    updateScore(scoreValue);
    setCurrentNumberOfMoves(currentNumberOfMoves + 1);
    setGoalReached(isGoalReached(goal, scoreValue));
  }

  const updateScore = (scoreValue: number): void => {
    setScore(scoreValue);
  }

  const markBlockedHexagons = (): void => {
    blockedHexagons.forEach(blockedHexagon => {
      blockedHexagon.selectedState();
      blockedHexagon.props.hexagon.setSelected(true);
    });
  }

  const unmarkBlockedHexagons = (): void => {
    blockedHexagons.forEach(blockedHexagon => {
      blockedHexagon.unselectedState();
      blockedHexagon.props.hexagon.setSelected(false);
    });
  }

  const reset = (): void => {
    setCurrentNumberOfMoves(0);
    setScore(0);
    setGoalReached(false);
    unmarkBlockedHexagons();
  }

  const calcUnblockingScores = (): Array<number> => {
    const unblockingScores: Array<number> = [];
    const unblockStep = Math.round(goal / (difficultyLevel + 1));
    unblockingScores.push(unblockStep);
    for (let i: number = 1; i < difficultyLevel; i++) {
      unblockingScores.push(Math.round(unblockStep) + unblockingScores[i - 1]);
    }
    return unblockingScores;
  }

  const isGoalReached = (goal: number, score: number): boolean => {
    return score >= goal;
  }

  const isScoreForUnblockingReached = (score: number, index: number) => {
    return score >= hexagonUnblocker[index].props.unblockScore;
  }

  // Get blocked SVGs hexagon objects for later highlighting
  const blockedHexagonSvgsCallback = (svgs: Array<HexagonSvg>) => {
    setBlockedHexagons(svgs);
  }

  // Get HexagonUnblocker objects for enabling and disabling
  const hexagonUnblockerCallback = (hexagonUnblocker: HexagonUnblocker) => {
    hexgonUnblockers.push(hexagonUnblocker);
    if (hexgonUnblockers.length === difficultyLevel) {
      setHexagonUnblocker(hexgonUnblockers);
    }
  }

  // On click handler for hexagon unblockers
  const hexagonUnblockerOnClickCallback = (hexagonUnblocker: HexagonUnblocker) => {
    if (hexagonUnblocker.isEnabled() && !hexagonUnblocker.isClicked()) {
      markBlockedHexagons();
      hexagonUnblocker.setClicked(true);
      setClickedHexagonUnblocker(hexagonUnblocker);
    } else if (hexagonUnblocker.isEnabled() && hexagonUnblocker.isClicked()) {
      unmarkBlockedHexagons();
      hexagonUnblocker.setClicked(false);
      setClickedHexagonUnblocker(undefined);
    }
  }

  const animateUnblock = (hexagon: Hexagon) => {
    animateHexagon(hexagon);
  }

  // To disable used unblockers
  const onClickBlockedHexagonSvgCallback = (hexagon: Hexagon) => {
    animateUnblock(hexagon);
    clickedHexagonUnblocker.setUsed(true);
    clickedHexagonUnblocker.setEnabled(false);
  }

  const createHexagonUnblockers = () => {
    const unblockScores: Array<number> = calcUnblockingScores();
    const hexagonUnblockers = [];
    for (let i = 0; i < difficultyLevel; i++) {
      hexagonUnblockers.push(<HexagonUnblocker key={i} unblockScore={unblockScores[i].toString()} hexagonUnblockerCallback={hexagonUnblockerCallback} onClickCallack={hexagonUnblockerOnClickCallback} />);
    }
    return hexagonUnblockers;
  }

  const updateCalculateMinimalNumberOfRequiredMoves = () => {
    setCalculateMinimalNumberOfRequiredMoves(!calculateMinimalNumberOfRequiredMoves);
    setCalculationOngoing(!calculationOngoing);
    setShowMinimalNumberOfRequiredMoves(true);
  }

  const renderMinimumNumberOfMovesCounter = () => {

    if (minimumNumberOfMoves > -1 && !calculationOngoing && showMinimalNumberOfRequiredMoves) {
      return <Counter value={minimumNumberOfMoves.toString()}>Min. number of moves</Counter>
    } else if (minimumNumberOfMoves === -1 && !calculationOngoing && showMinimalNumberOfRequiredMoves) {
      return <Counter value={'No solution with the current state'}>Min. number of moves</Counter>
    } else if (calculationOngoing) {
      return <Spinner animation="border" role="status" style={{ alignSelf: 'center' }}>
        <span className="sr-only">Loading...</span>
      </Spinner>
    } else if (!calculateMinimalNumberOfRequiredMoves && !calculationOngoing && !showMinimalNumberOfRequiredMoves) {
      return <div>
        <Counter value={''}>Min. number of moves</Counter>
        <Button variant='outline-dark' style={{ padding: '0.5em', marginBottom: '1em' }} onClick={updateCalculateMinimalNumberOfRequiredMoves}>
          <svg className="bi bi-question-circle" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z" clipRule="evenodd" />
            <path d="M5.25 6.033h1.32c0-.781.458-1.384 1.36-1.384.685 0 1.313.343 1.313 1.168 0 .635-.374.927-.965 1.371-.673.489-1.206 1.06-1.168 1.987l.007.463h1.307v-.355c0-.718.273-.927 1.01-1.486.609-.463 1.244-.977 1.244-2.056 0-1.511-1.276-2.241-2.673-2.241-1.326 0-2.786.647-2.754 2.533zm1.562 5.516c0 .533.425.927 1.01.927.609 0 1.028-.394 1.028-.927 0-.552-.42-.94-1.029-.94-.584 0-1.009.388-1.009.94z" />
          </svg>
        </Button>
      </div>
    }
  }

  return (
    <div className="App">
      <Header></Header>
      {difficultyLevel > -1 ? (<Container fluid={true}>
        <Row>
          <Col sm={9} className='App-game-container'>
            <HexagonGrid difficultyLevel={difficultyLevel} goal={goal} calculateMinimalNumberOfRequiredMoves={calculateMinimalNumberOfRequiredMoves} setCalculateMinimalNumberOfRequiredMoves={setCalculateMinimalNumberOfRequiredMoves} setCalculationOngoing={setCalculationOngoing} updateScore={updateScore} onChange={onChangeHandler} reset={reset} goalReached={goalReached} blockedHexagonSvgsCallback={blockedHexagonSvgsCallback} onClickBlockedHexagonSvgCallback={onClickBlockedHexagonSvgCallback} setMinimumNumberOfMoves={setMinimumNumberOfMoves} />
          </Col>
          <Col sm={3} className='App-counter-container'>
            <Counter value={goal.toString()}>Goal</Counter>
            <Row className='hexagon-unblocker-container justify-content-center'>
              {createHexagonUnblockers()}
            </Row>
            <Counter value={score.toString()}>Score</Counter>
            {renderMinimumNumberOfMovesCounter()}
            <Counter value={currentNumberOfMoves.toString()}>Current number of moves</Counter>
          </Col>
        </Row>
      </Container>) : (<Redirect to='/' />)}
    </div>
  );
}

export default React.memo(GamePage);