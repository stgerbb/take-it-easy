import { Table, Coordinates, Corner, CardValues } from './Interfaces';

export default class Hexagon extends Array<Corner> {

    private id: string;
    private selected: boolean = false;
    private centerPoint: Coordinates;
    private drawingStartPoint: Coordinates;
    cardValues: CardValues;
    gridPosition: Table;


    constructor(id: string, gridPosition: Table, size: number, drawingStartPoint: Coordinates, cardValues: CardValues) {
        super();
        this.id = id;
        this.gridPosition = gridPosition;
        this.gridPosition.s = this.gridPosition.column * -1 - this.gridPosition.row;
        this.centerPoint = this.gridToCoordinates(gridPosition, size);
        this.cardValues = cardValues;
        this.drawingStartPoint = drawingStartPoint;
        this.calcCornersOfHexagon(this.centerPoint, size, drawingStartPoint);
    }

    private gridToCoordinates = (grid: Table, size: number): Coordinates => {
        const x: number = size * (3. / 2 * grid.column);
        const y: number = size * (Math.sqrt(3) / 2 * grid.column + Math.sqrt(3) * grid.row)
        return { x, y };
    }

    calcCornerOfHexagon = (centerPoint: Coordinates, size: number, i: number, drawingStartPoint: Coordinates): Coordinates => {
        const angleDeg: number = 60 * i;
        const angleRad: number = Math.PI / 180 * angleDeg;
        const x: number = centerPoint.x + size * Math.cos(angleRad) + drawingStartPoint.x;
        const y: number = centerPoint.y + size * Math.sin(angleRad) + drawingStartPoint.y;
        return { x, y };
    }

    private calcCornersOfHexagon = (centerPoint: Coordinates, size: number, drawingStartPoint: Coordinates): void => {
        for (let i: number = 0; i < 6; i++) {
            const cornerCoordinates: Coordinates = this.calcCornerOfHexagon(centerPoint, size, i, drawingStartPoint);
            this.push({ coordinates: cornerCoordinates });
        }
    }

    calculateMiddelBetweenCornersOnConnectingLine = (startCorner: Corner, endCorner: Corner): Coordinates => {
        const x: number = (startCorner.coordinates.x + endCorner.coordinates.x) / 2
        const y: number = (startCorner.coordinates.y + endCorner.coordinates.y) / 2
        return { x, y }
    }

    // Start & end points per axis to draw lines in hexagon
    getXAxisStartCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[2], this[3]);
    }

    getXAxisEndCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[5], this[0]);
    }

    getYAxisStartCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[0], this[1]);
    }

    getYAxisEndCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[3], this[4]);
    }

    getZAxisStartCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[4], this[5]);
    }

    getZAxisEndCoord = (): Coordinates => {
        return this.calculateMiddelBetweenCornersOnConnectingLine(this[1], this[2]);
    }

    getHexagonCenter = (): Coordinates => {
        return { x: this.centerPoint.x + this.drawingStartPoint.x, y: this.centerPoint.y + this.drawingStartPoint.y };
    }

    isSelected = (): boolean => this.selected;

    isBlocked = (): boolean => this.getCardValues().blocked;

    setBlocked = (bool: boolean): boolean => this.getCardValues().blocked = bool;

    setSelected = (bool: boolean) => this.selected = bool;

    getGridPosition = (): Table => this.gridPosition;
    
    setGridPosition = (gridPostion: Table): void => {this.gridPosition = gridPostion};

    getCardValues = (): CardValues => this.cardValues;

    setCardValues = (cardValues: CardValues) => this.cardValues = cardValues;

    getId = (): string => this.id;
}