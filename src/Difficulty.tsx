export enum Difficulty {
    Easy = 4,
    Medium = 6,
    Hard = 8,
}