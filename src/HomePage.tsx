import React, { FunctionComponent } from 'react'
import { Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Footer } from './components/Footer';
import { Difficulty } from './Difficulty';
import { calculateRandomNumber } from './GamePage';

import './HomePage.css'


interface HomePageProps {
    setDifficultyLevel: (difficultyLevel: number) => void
}

const HomePage: FunctionComponent<HomePageProps> = ({ setDifficultyLevel }) => {

    const setCustomDifficulty = (event: React.FormEvent<HTMLSelectElement>) => {
        setDifficultyLevel(parseInt(event.currentTarget.value, 10));
    }

    const initDifficulty = (difficultyLevel: number): number => {
        switch (difficultyLevel) {
            case Difficulty.Easy:
                return calculateRandomNumber(0, 4);
            case Difficulty.Medium:
                return calculateRandomNumber(5, 7);
            case Difficulty.Hard:
                return calculateRandomNumber(8, 10);
            default:
                return 0;
        }
    }

    return (
        <div className="App">
            <h1 className="title">Take it easy!</h1>
            <div className="difficulty">
                <h1>Choose a difficulty:</h1>
                <Link className="difficulty-level-link" to="/game" onClick={() => setDifficultyLevel(initDifficulty(Difficulty.Easy))}><h2 className="difficulty-level">Easy</h2></Link>
                <Link className="difficulty-level-link" to="/game" onClick={() => setDifficultyLevel(initDifficulty(Difficulty.Medium))}><h2 className="difficulty-level">Medium</h2></Link>
                <Link className="difficulty-level-link" to="/game" onClick={() => setDifficultyLevel(initDifficulty(Difficulty.Hard))}><h2 className="difficulty-level">Hard</h2></Link>
                <Form className="custom-difficulty">
                    <Form.Group controlId='custom-difficulty.ControlSelect'>
                        <h1 id='select-container'>
                            <Form.Label>Custom: </Form.Label>
                            <div id='custom-select'>
                                <Form.Control as='select' onChange={setCustomDifficulty} >
                                    <option value={1}>1</option>
                                    <option value={2}>2</option>
                                    <option value={3}>3</option>
                                    <option value={4}>4</option>
                                    <option value={5}>5</option>
                                    <option value={6}>6</option>
                                    <option value={7}>7</option>
                                    <option value={8}>8</option>
                                    <option value={9}>9</option>
                                    <option value={10}>10</option>
                                </Form.Control>
                            </div>
                            <Link className="difficulty-level-link" to="/game">
                                Go!
                            </Link>
                        </h1>
                    </Form.Group>
                </Form>
            </div>
            <Footer></Footer>
        </div>
    )
}

export default React.memo(HomePage);