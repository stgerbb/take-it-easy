import { expose } from 'comlink';
import { calculateMinimalNumberOfMovesToReachGoal } from '../Backtracking';

const exports = {
    calculateMinimalNumberOfMovesToReachGoal
};
export type CalculationWorker = typeof exports;

expose(exports);