import React, { FunctionComponent } from 'react'

import Grid from './Grid';
import Hexagons from './Hexagons';
import { Coordinates } from './Interfaces';
import { For } from 'react-loops';
import HexagonSvg from './HexagonSvg';
import HexagonSvgCache from './HexagonSvgCache';
import Hexagon from './Hexagon';
import { calculateGridScore, visualiseCompleteLines } from './GridUtils';
import WinModal from './components/WinModal';
import { useCalculateMinimalNumberOfMovesToReachGoal } from './App.hooks';
import { animateHexagon } from './animateUtils';

interface HexagonGridProps {
    updateScore: (scoreValue: number) => void,
    onChange: (scoreValue: number) => void,
    reset: () => void,
    blockedHexagonSvgsCallback: (blockedSvgs: Array<HexagonSvg>) => void,
    onClickBlockedHexagonSvgCallback: (hexagon: Hexagon) => void,
    setMinimumNumberOfMoves: (minimumNumberOfMoves: number) => void,
    setCalculateMinimalNumberOfRequiredMoves: (calculateMinimalNumberOfRequiredMoves: boolean) => void,
    setCalculationOngoing: (calculationOngoing: boolean) => void,
    difficultyLevel: number,
    goal: number,
    goalReached: boolean,
    calculateMinimalNumberOfRequiredMoves: boolean,
}

const HexagonGrid: FunctionComponent<HexagonGridProps> = ({ updateScore, onChange, reset, blockedHexagonSvgsCallback, onClickBlockedHexagonSvgCallback, setMinimumNumberOfMoves, setCalculateMinimalNumberOfRequiredMoves, setCalculationOngoing, difficultyLevel, goal, goalReached, calculateMinimalNumberOfRequiredMoves: calculateMinimalNumberOfRequiredMoves }) => {

    const MAX_SELECTED_COUNT: number = 2

    const grid: Grid = new Grid();
    const size: number = 70;
    const svgWidth: number = ((2 * size) * 3) + (2 * size) + 1;
    const svgHeight: number = ((Math.sqrt(3) * size) * 5) + 2;
    const drawingStartPoint: Coordinates = { x: svgWidth / 2, y: Math.sqrt(3) * (size / 2) + 1 }
    const svgCache: HexagonSvgCache = new HexagonSvgCache();

    const setUpGameWithScoreLowerThanGoal = (): Hexagons => {
        let scoreHigerThanGoal: boolean = true;
        let interimHexagons: Hexagons
        while(scoreHigerThanGoal) {
            interimHexagons = new Hexagons(grid, size, difficultyLevel, drawingStartPoint)
            const currentScore: number = calculateGridScore(grid, interimHexagons);
            scoreHigerThanGoal = currentScore > goal;
        }
        return interimHexagons;
    }

    const [showModal, setShowModal] = React.useState(goalReached);
    const [hexagons, setHexagons] = React.useState(setUpGameWithScoreLowerThanGoal())
    const [blockedSvgs, setBlockedSvgs] = React.useState([]);

    const hexagonsCopy = JSON.parse(JSON.stringify(hexagons));
    hexagonsCopy.forEach((hexagon: Hexagon, index: number) => {
        hexagon.cardValues = hexagons[index].cardValues;
        hexagon.gridPosition = hexagons[index].gridPosition;
    });
    const calculationResult = useCalculateMinimalNumberOfMovesToReachGoal(goal, hexagonsCopy, grid, calculateMinimalNumberOfRequiredMoves);

    React.useEffect(() => {
        setMinimumNumberOfMoves(calculationResult.result);
        setCalculationOngoing(calculationResult.isCalculating)
    }, [calculationResult]);

    React.useEffect(() => {
        updateScore(calculateGridScore(grid, hexagons));
        visualiseCompleteLines(grid, hexagons);
    }, []);

    React.useEffect(() => {
        setShowModal(goalReached);
    }, [goalReached]);

    React.useEffect(() => {
        if (calculateMinimalNumberOfRequiredMoves) {
            setCalculateMinimalNumberOfRequiredMoves(!calculateMinimalNumberOfRequiredMoves);
        }
    }, [calculateMinimalNumberOfRequiredMoves]);

    // Previous code
    // const updateHexagons = () => {
    //     const hexagons: Hexagons = new Hexagons(grid, size, difficultyLevel, drawingStartPoint);
    //     setHexagons(hexagons);
    //     return hexagons;
    // }



    const initialiseNewGame = () => {
        // In meiner Lösung ist es so, dass beim rendering der HexagonGrid Klasse mittels einem Callback die gesperrten SVGs ermittelt werden.
        // Beim neustarten eines Spiels kann ich das nicht über State/Props wieder reseten, sondern ein re-rendering ist nötig.
        // Bei React Components gibt es die Methode 'forceUpdate()' die ein re-rendering triggern würde. Bei FunctionComponents ist es mir 
        // nicht ganz klar wie man ein forceUpdate() umsetzt. Es gibt mehrere Möglichkeiten und ich kann nicht abwägen, welche gut/schlecht ist ->
        // siehe Link: https://stackoverflow.com/questions/53215285/how-can-i-force-component-to-re-render-with-hooks-in-react
        // Um ein re-rendering zu triggern, lade ich demzufolge im Moment einfach die Seite neu.
        window.location.reload();

        // Previous code
        // const hexagons: Hexagons = updateHexagons();
        // reset();
        // updateScore(calculateGridScore(grid, hexagons));
        // visualiseCompleteLines(grid, hexagons);
    };

    // Get blocked SVG hexagons
    const getBlockedHexagonSvgs = (svg: HexagonSvg): void => {
        blockedSvgs.push(svg);
        if (blockedSvgs.length === difficultyLevel) {
            blockedHexagonSvgsCallback(blockedSvgs);
        }
    }

    const unmarkBlockedHexagons = (): void => {
        blockedSvgs.forEach(element => {
            element.props.hexagon.setSelected(false);
            element.unselectedState();
        });
    }

    const removeBlockedSvgFromArray = (svg: HexagonSvg): void => {
        const index = blockedSvgs.indexOf(svg, 0);
        if (index > -1) {
            blockedSvgs.splice(index, 1);
        }
    }

    const animateSwap = (hexagons: Hexagon[]) => {
        hexagons.map(((hexagon: Hexagon) => animateHexagon(hexagon)))
    }

    const onClickHexagonCallback = (svg: HexagonSvg): void => {
        const clickedHexagon = svg.props.hexagon;
        if (blockedSvgs.length > 0 && blockedSvgs[0].props.hexagon.isSelected()) {
            if (clickedHexagon.isBlocked()) {
                clickedHexagon.setBlocked(false);
                unmarkBlockedHexagons();
                removeBlockedSvgFromArray(svg);
                onClickBlockedHexagonSvgCallback(clickedHexagon);
            }
        } else {
            if (clickedHexagon.isBlocked()) {
                return;
            } else if (clickedHexagon.isSelected()) {
                svg.unselectedState();
                clickedHexagon.setSelected(false)
                svgCache.clear();
            } else {
                svgCache.push(svg);
                svg.selectedState();
                clickedHexagon.setSelected(true);
            }
        }
        if (hexagons.getSelectedHexagons().length === MAX_SELECTED_COUNT) {
            svgCache.forEach(element => {
                element.unselectedState();
            });
            hexagons.unselectAllHexagons();
            const selectedHexagon1: Hexagon = svgCache[0].props.hexagon;
            const selectedHexagon2: Hexagon = svgCache[1].props.hexagon;
            animateSwap([selectedHexagon2, selectedHexagon1]);
            hexagons.swapHexagonContent(selectedHexagon1, selectedHexagon2);
            onChange(calculateGridScore(grid, hexagons))
            visualiseCompleteLines(grid, hexagons);
            svgCache.clear();
        }
    }

    return (
        <div>
            <WinModal show={showModal} setShow={setShowModal} initialiseNewGame={initialiseNewGame} />
            <svg width={svgWidth} height={svgHeight}>
                <For of={hexagons} as={hexagon =>
                    <HexagonSvg id={hexagon.getId()} hexagon={hexagon} width={svgWidth} height={svgHeight} onClickCallback={onClickHexagonCallback} blockedHexagonCallback={getBlockedHexagonSvgs} />
                } />
            </svg>
        </div>
    );
}

export default React.memo(HexagonGrid);