export const colorDictionary: {[cardValue: number]: string} = {
    1: 'grey',
    2: 'purple',
    3: 'violet',
    4: 'turquoise',
    5: 'blue',
    6: 'red',
    7: 'green',
    8: 'orange',
    9: 'yellow'
}