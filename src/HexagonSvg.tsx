import React, { Component } from 'react';
import Hexagon from './Hexagon';
import { colorDictionary } from './ColorDict'
import { JSXNode } from 'react-loops';
import { createLineIdFromGridPositionAndAxis } from './GridUtils';
import './HexagonSvg.css';
import { Corner } from './Interfaces';

interface HexagonSvgProps {
    id: string
    hexagon: Hexagon,
    width: number,
    height: number,
    onClickCallback: (svg: HexagonSvg) => void,
    blockedHexagonCallback: (svg: HexagonSvg) => void
}

interface HexagonSvgState {
    strokeColor: string,
    strokeWidth: number
}

export default class HexagonSvg extends Component<HexagonSvgProps, HexagonSvgState> {

    private readonly LINE_STROKE_WIDTH: number = 2;

    constructor(props: HexagonSvgProps) {
        super(props);
        this.state = {
            strokeColor: '#000',
            strokeWidth: 1
        }

        this.renderLockForBlockedHexagon = this.renderLockForBlockedHexagon.bind(this);
    }

    componentDidMount() {
        if (this.props.hexagon.isBlocked()) {
            this.props.blockedHexagonCallback(this);
        }
    }

    selectedState = (): void => {
        this.setState({
            strokeColor: '#6600FF',
            strokeWidth: 5
        });
    }

    unselectedState = (): void => {
        this.setState({
            strokeColor: '#000',
            strokeWidth: 1
        });
    }

    renderLockForBlockedHexagon = (hexagon: Hexagon): JSXNode => {
        if (hexagon.getCardValues().blocked) {
            const imageDimension: number = 60;
            return (
                <image
                    style={{ opacity: "70%" }}
                    href='https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/OOjs_UI_icon_lock.svg/1200px-OOjs_UI_icon_lock.svg.png'
                    x={hexagon.getHexagonCenter().x - imageDimension / 2}
                    y={hexagon.getHexagonCenter().y - imageDimension / 2}
                    height={imageDimension}
                    width={imageDimension}
                >
                </image>
            );
        }
    }

    generatePolygonPointsString(hexagon: Hexagon) {
        let polygonPointsString = ``
        hexagon.forEach((corner: Corner) => polygonPointsString += `${corner.coordinates.x}, ${corner.coordinates.y} `);
        return polygonPointsString;
    }

    render() {
        return (
            <svg xmlns="http://www.w3.org/2000/svg" width={this.props.width} height={this.props.height}>
                <g className={this.props.hexagon.getId()}>
                    <polygon
                        onClick={() => this.props.onClickCallback(this)}
                        fill="#FFF"
                        stroke={this.state.strokeColor}
                        strokeWidth={this.state.strokeWidth}
                        points={this.generatePolygonPointsString(this.props.hexagon)}
                    />
                    <line
                        id={createLineIdFromGridPositionAndAxis(this.props.hexagon.getGridPosition(), 'x')}
                        x1={this.props.hexagon.getXAxisStartCoord().x}
                        y1={this.props.hexagon.getXAxisStartCoord().y}
                        x2={this.props.hexagon.getXAxisEndCoord().x}
                        y2={this.props.hexagon.getXAxisEndCoord().y}
                        stroke={colorDictionary[this.props.hexagon.getCardValues().x]}
                        strokeWidth={this.LINE_STROKE_WIDTH}
                    />
                    <line
                        id={createLineIdFromGridPositionAndAxis(this.props.hexagon.getGridPosition(), 'y')}
                        x1={this.props.hexagon.getYAxisStartCoord().x}
                        y1={this.props.hexagon.getYAxisStartCoord().y}
                        x2={this.props.hexagon.getYAxisEndCoord().x}
                        y2={this.props.hexagon.getYAxisEndCoord().y}
                        stroke={colorDictionary[this.props.hexagon.getCardValues().y]}
                        strokeWidth={this.LINE_STROKE_WIDTH}
                    />
                    <line
                        id={createLineIdFromGridPositionAndAxis(this.props.hexagon.getGridPosition(), 'z')}
                        x1={this.props.hexagon.getZAxisStartCoord().x}
                        y1={this.props.hexagon.getZAxisStartCoord().y}
                        x2={this.props.hexagon.getZAxisEndCoord().x}
                        y2={this.props.hexagon.getZAxisEndCoord().y}
                        stroke={colorDictionary[this.props.hexagon.getCardValues().z]}
                        strokeWidth={this.LINE_STROKE_WIDTH}
                    />
                    <text x={this.props.hexagon.getXAxisStartCoord().x + 3}
                        y={this.props.hexagon.getXAxisStartCoord().y - 10}>
                        {this.props.hexagon.getCardValues().x.toString()}
                    </text>
                    <text x={this.props.hexagon.getYAxisStartCoord().x - 10}
                        y={this.props.hexagon.getYAxisStartCoord().y - 10}>
                        {this.props.hexagon.getCardValues().y.toString()}
                    </text>
                    <text x={this.props.hexagon.getZAxisStartCoord().x + 10}
                        y={this.props.hexagon.getZAxisStartCoord().y + 20}>
                        {this.props.hexagon.getCardValues().z.toString()}
                    </text>
                    {this.renderLockForBlockedHexagon(this.props.hexagon)}
                </g>
            </svg>
        );
    }
}