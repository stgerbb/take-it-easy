import { Table } from "./Interfaces";
import Hexagon from "./Hexagon";
import Hexagons from "./Hexagons";
import Grid from "./Grid";
import _ from "lodash";

const createIdFromGridPosition = (gridPosition: Table): string => {
    return `hexagon-${gridPosition.column}${gridPosition.row}`
}

export const createLineIdFromGridPositionAndAxis = (gridPosition: Table, axis: string): string => {
    return `${createIdFromGridPosition(gridPosition)}-${axis}-axis`
}

export const calculateGridScore = (grid: Grid, hexagons: Hexagons): number => {
    const diagonalScore = calculateDiagonalScore(grid, hexagons);
    const rowScore = calculateRowScore(grid, hexagons);
    const columnScore = calculateColumnScore(grid, hexagons);
    return diagonalScore + rowScore + columnScore;
}

const calculateScore = (numbers: number[]): number => {
    return numbers[0] * numbers.length;
}

const calculateAxisScore = (hexagons: Hexagons, axis: string, axisPositions: _.Dictionary<Array<Table>>): number => {
    let axisScore: number = 0;
    for (const [, axisPosition] of Object.entries(axisPositions)) { // only 'axisPosition' is used. If key is needed, replace [, axisPosition] with [key, axisPosition]
        const hexagonsAtPosition: Array<Hexagon> = getHexagonsAtPositions(hexagons, axisPosition);
        const cardValues: number[] = extractRelevantCardValue(axis, hexagonsAtPosition);
        if (numbersInArraySame(cardValues)) {
            axisScore += calculateScore(cardValues);
        }
    }
    return axisScore;
}

const calculateDiagonalScore = (grid: Grid, hexagons: Hexagons): number => {
    const gridDiagonals = getGridAxis(grid, 's');
    return calculateAxisScore(hexagons, 'x', gridDiagonals);
}

const calculateRowScore = (grid: Grid, hexagons: Hexagons): number => {
    const gridRows = getGridAxis(grid, 'row');
    return calculateAxisScore(hexagons, 'y', gridRows);
}

const calculateColumnScore = (grid: Grid, hexagons: Hexagons): number => {
    const gridColumns = getGridAxis(grid, 'column');
    return calculateAxisScore(hexagons, 'z', gridColumns);
}

export const visualiseCompleteLines = (grid: Grid, hexagons: Hexagons): void => {
    visualiseCompleteDiagonalLines(grid, hexagons);
    visualiseCompleteRowLines(grid, hexagons);
    visualiseCompleteColumnLines(grid, hexagons);
}

const visualiseCompleteDiagonalLines = (grid: Grid, hexagons: Hexagons): void => {
    const gridDiagonals = getGridAxis(grid, 's');
    visualiseCompleteLineAlongAxis(hexagons, 'x', gridDiagonals);
}

const visualiseCompleteRowLines = (grid: Grid, hexagons: Hexagons): void => {
    const gridDiagonals = getGridAxis(grid, 'row');
    visualiseCompleteLineAlongAxis(hexagons, 'y', gridDiagonals);
}

const visualiseCompleteColumnLines = (grid: Grid, hexagons: Hexagons): void => {
    const gridDiagonals = getGridAxis(grid, 'column');
    visualiseCompleteLineAlongAxis(hexagons, 'z', gridDiagonals);
}

const visualiseCompleteLineAlongAxis = (hexagons: Hexagons, axis: string, axisPositions: _.Dictionary<Array<Table>>): void => {
    for (const [, axisPosition] of Object.entries(axisPositions)) { // only 'axisPosition' is used. If key is needed, replace [, axisPosition] with [key, axisPosition]
        const hexagonsAtPosition: Array<Hexagon> = getHexagonsAtPositions(hexagons, axisPosition);
        const cardValues: number[] = extractRelevantCardValue(axis, hexagonsAtPosition);
        hexagonsAtPosition.map((hexagon: Hexagon) => {
            const id = createLineIdFromGridPositionAndAxis(hexagon.getGridPosition(), axis);
            const lineElement: HTMLElement = getLineElementById(id);
            if (numbersInArraySame(cardValues)) {
                lineElement.classList.add('complete-line');
            } else {
                lineElement.classList.remove('complete-line');
            }
        });
    }
}

const getLineElementById = (id: string): HTMLElement => {
    return document.getElementById(id);
}

const extractRelevantCardValue = (axis: string, hexagons: Array<Hexagon>): number[] => {
    return hexagons.map((hexagon: Record<string, any>) => hexagon.cardValues[axis]);
}

const numbersInArraySame = (numbers: number[]): boolean => {
    return numbers.every((number: number) => number === numbers[0]);
}

const getGridAxis = (grid: Grid, axis: string): _.Dictionary<Array<Table>> => {
    return _.groupBy(grid, axis);
}

const getHexagonsAtPositions = (hexagons: Hexagons, positions: Array<Table>): Array<Hexagon> => {
    return positions.map((position: Table) => getHexagonAtGridCoordinate(hexagons, position));
}

const getHexagonAtGridCoordinate = (hexagons: Hexagons, gridCoordinate: Table): Hexagon => {
    return hexagons.find((hexagon: Hexagon) => {
        const gridPosition = hexagon.gridPosition;
        return gridPosition.column === gridCoordinate.column && gridPosition.row === gridCoordinate.row;
    })
}