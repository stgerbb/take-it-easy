export interface Table {
    column: number;
    row: number;
    s?: number;
}

export interface Coordinates {
    x: number;
    y: number;
}

export interface Corner {
    coordinates: Coordinates;
}

export interface CardValues {
    x: number;
    y: number;
    z: number;
    blocked: boolean;
}